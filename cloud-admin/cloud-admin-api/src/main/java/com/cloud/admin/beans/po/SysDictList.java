/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.admin.beans.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.cloud.common.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典项list
 *
 * @author Aijm
 * @date 2019-09-05 19:52:37
 */
@Data
@TableName("sys_dict_list")
@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "字典项list")
public class SysDictList extends BaseEntity<SysDictList> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字段名称key")
    private String name;

    @ApiModelProperty(value = "值value")
    private String value;

    @ApiModelProperty(value = "标签")
    private String label;

    @ApiModelProperty(value = "编码类型")
    private String typeCode;

    @ApiModelProperty(value = "排序（升序）")
    private Integer sort;


}