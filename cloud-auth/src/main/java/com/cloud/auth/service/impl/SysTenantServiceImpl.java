/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.auth.service.impl;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.cloud.auth.entity.SysTenant;
import com.cloud.auth.mapper.SysTenantMapper;
import com.cloud.auth.service.SysTenantService;
import com.cloud.common.cache.annotation.CachePut;
import com.cloud.common.cache.constants.CacheScope;
import com.cloud.common.data.base.BaseService;
import org.springframework.stereotype.Service;

/**
 * 租户管理
 *
 * @author Aijm
 * @date 2020-05-25 13:32:23
 */
@Service
public class SysTenantServiceImpl extends BaseService<SysTenantMapper, SysTenant> implements SysTenantService {


    /**
     * 获取到租户信息
     *
     * @param clientId
     * @return
     */
    @Override
    @CachePut(scope = CacheScope.TENTANT_KEY, key = "#clientId")
    public SysTenant getSysTenant(String clientId) {
        return super.getOne(Wrappers.<SysTenant>query().lambda().eq(SysTenant::getClientId, clientId));
    }
}