/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.data.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.common.entity.BaseEntity;

/**
 * @Author Aijm
 * @Description 接口baseMapper
 * @Date 2019/10/12
 */
public interface ProMapper<T extends BaseEntity> extends BaseMapper<T> {

}