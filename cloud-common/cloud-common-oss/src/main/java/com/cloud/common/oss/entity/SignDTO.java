/**
 *    https://gitee.com/gitsc/pro-cloud/
 *     @Author Aijm 2929793435@qq.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.cloud.common.oss.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author Aijm
 * @Description 签名传递参数
 * @Date 2019/9/12
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignDTO implements Serializable {
    private static final long serialVersionUID=1L;

    /**
     * accessKeyId
     */
    private String accessKeyId;

    /**
     *
     */
    private String policy;

    /**
     * 签名
     */
    private String signature;

    /**
     * 存储的目录
     */
    private String dir;

    /**
     * 回调地址
     */
    private String callback;

    /**
     * 返回过期时间
     */
    private Date expire;
    /**
     *
     */
    private String action;

}